package com.example

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class Dates {
	private String getAppointmentDate(Date d, Number n) {
		d = d + n
		String appointmentDate = d.format("dd/MM/yyyy", TimeZone.getTimeZone('Pacific/Auckland'))
		return appointmentDate
	}

	@Keyword
	def getgetAppointmentDateFromDateNow(Number n) {
		Date now = new Date()
		return getAppointmentDate(now, n)
	}

	@Keyword
	def getgetAppointmentDateFromDate(String dateStr, Integer n) {

		Date d = Date.parse("E MMM dd H:m:s z yyyy", dateStr)
		return getAppointmentDate(d, n)
	}
}