import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.selectOptionByLabel(findTestObject('pageCuraAppointment/facility'), facility, false)

'If readmission then tick readmission'
if (readmission) {
    WebUI.check(findTestObject('pageCuraAppointment/readmission'))
}

switch (healthcareProgram) {
    case healthcareProgram == 'Medicare':
        'Case of Medicare then check medicare radio button'
        WebUI.check(findTestObject('pageCuraAppointment/medicare'))

        break
    case healthcareProgram == 'Medicaid':
        'Case of Medicare then check medicaid radio button'
        WebUI.check(findTestObject('pageCuraAppointment/medicaid'))

        break
    default:
        'Check none radio button as default'
        WebUI.check(findTestObject('pageCuraAppointment/none'))

        break
}

'Determine date base on today plus a n days'

println (visitInDays)

String visitDate = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDateNow'(visitInDays.toInteger())

WebUI.setText(findTestObject('pageCuraAppointment/visitDate'), visitDate)

WebUI.setText(findTestObject('pageCuraAppointment/comment'), comment)

WebUI.takeScreenshot()

WebUI.click(findTestObject('pageCuraAppointment/bookAppointment'))

WebUI.verifyTextPresent('Appointment Confirmation', false)

WebUI.takeScreenshot()

WebUI.verifyMatch(facility, WebUI.getText(findTestObject('pageAppointmentConfirmation/facility')), false)

if (readmission) {
    'Verify Yes if readmission'
    WebUI.verifyMatch('Yes', WebUI.getText(findTestObject('pageAppointmentConfirmation/hospitalReadmission')), false)
} else {
    'Verify No if readmission'
    WebUI.verifyMatch('No', WebUI.getText(findTestObject('pageAppointmentConfirmation/hospitalReadmission')), false)
}

WebUI.verifyMatch(healthcareProgram, WebUI.getText(findTestObject('pageAppointmentConfirmation/program')), false)

WebUI.verifyMatch(visitDate, WebUI.getText(findTestObject('pageAppointmentConfirmation/visitDate')), false)

WebUI.verifyMatch(comment, WebUI.getText(findTestObject('pageAppointmentConfirmation/comment')), false)

WebUI.takeScreenshot()

