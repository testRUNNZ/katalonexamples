import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.comment('Text Today keyword return today + number of days')

def todaysAppointment = ''

'31-Dec-2018 + 0 days'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDate'('Mon Dec 31 00:00:00 UTC 2018', 0)

println(todaysAppointment)

assert todaysAppointment == '31/12/2018'

'1-Jan-2018 + 0 days'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDate'('Mon Jan 1 00:00:00 UTC 2018', 0)

println(todaysAppointment)

assert todaysAppointment == '01/01/2018'

'31-Dec-2018 + 1 days'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDate'('Mon Dec 31 00:00:00 UTC 2018', 1)

println(todaysAppointment)

assert todaysAppointment == '01/01/2019'

'1-Jan-2018 + 1 days'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDate'('Mon Jan 1 00:00:00 UTC 2018', 1)

println(todaysAppointment)

assert todaysAppointment == '02/01/2018'

'Today - check println result'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDateNow'(0)

println(todaysAppointment)

'Today + 1 day - check println result'
todaysAppointment = CustomKeywords.'com.example.Dates.getgetAppointmentDateFromDateNow'(0)

println(todaysAppointment)

