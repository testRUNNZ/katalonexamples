import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import org.openqa.selenium.Keys as Keys

WebUI.comment('Story: Book an appointment')

WebUI.verifyCheckpoint(findCheckpoint('Checkpoints/AppointmentCheckpoint'), true)

'Given that the user has the valid login information'
WebUI.callTestCase(findTestCase('Blocks/Open'), [:], FailureHandling.STOP_ON_FAILURE)

'When he logins to CURA system'
WebUI.callTestCase(findTestCase('Blocks/Login'), [('username') : username, ('passwordEncypted') : passwordEncypted], FailureHandling.STOP_ON_FAILURE)

'Then he should be able to login successfully'
landingPage = WebUI.verifyElementPresent(findTestObject('pageCuraAppointment/appointment'), 5)

WebUI.callTestCase(findTestCase('Blocks/BookAppointment'), [('facility') : facility, ('readmission') : readmission, ('healthcareProgram') : healthcareProgram
        , ('visitInDays') : visitInDays, ('comment') : comments], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Blocks/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

