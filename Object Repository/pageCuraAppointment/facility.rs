<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>facility</name>
   <tag></tag>
   <elementGuidId>4f65af4a-3b80-46ed-a3b4-9a3ee9fbf8cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'combo_facility' and @type = 'select']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>combo_facility</value>
   </webElementProperties>
</WebElementEntity>
